# myshell

## Name
My Shell

## Description
A dedicated repo for all my shell aliases, functions, exports, etc. Use/borrow/modify at will!

## Installation

### Fedora-like (uses .bashrc.d dropins)

The ultra simple installer creates `.bashrc.d/` symlinks to the relevant repo files.

```
./install
```